<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

class IsTutorOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = request()->user()->getRoleNames()->toArray();
        if (in_array('Admin', $roles) || in_array('Tutor', $roles) ) {
            return $next($request);
        } else {
            return response([
                'message' => 'Insufficient Permissions'
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
}
