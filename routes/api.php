<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::group([
//    'prefix' => 'tutores',
//    'middleware' => 'api'
//], function () {
//    Route::get('/index', 'AdminController@showTutores');
//});

//Auth Routes
Route::post('/login', 'AuthController@login');
Route::middleware('auth:sanctum')->group(function () {
    //* Auth Routes
    Route::get('/user', 'AuthController@user');
    Route::post('/logout', 'AuthController@logout');

    //* Print Routes
    Route::get('/printRepInd/{id}', 'TutorController@printrepInd');
    Route::get('/printRepGrup/{id}', 'TutorController@printrepGrup');

    //* Anuncios
    Route::get('/anuncios', 'AdminController@anuncios');
    Route::get('/getImagen/{fileName}', 'AdminController@getImagen');

});

Route::group(['middleware' => ['auth:sanctum', 'isadmin'], 'prefix' => 'admin'], function () {
    //general

    Route::get('/perfil', 'AdminController@perfil');
    Route::get('/editPerfil', 'AdminController@editPerfil');
    Route::post('/updatePerfil', 'AdminController@updatePerfil');
    //tutores
    Route::post('/storeTutor', 'AdminController@storeTutor');
    Route::post('/actualizarTutor/{id}', 'AdminController@actualizarTutor');
    Route::get('/tutores', 'AdminController@showTutores');
    Route::get('/tutores/{id}', 'AdminController@getTutor');
    Route::get('/alumnosMonitores/{id}', 'AdminController@showMonitoresTutor'); //pending
    Route::get('/crearPsicologo', 'AdminController@crearPsicologo');
    Route::post('/storePsicologo', 'AdminController@storePsicologo');
    Route::get('/psicologos', 'AdminController@showPsicologos');
    //Reportes Grupales
    Route::get('/reportesGrupales', 'AdminController@reportesGrupales');
    Route::get('/reportesIndividuales', 'AdminController@reportesIndividuales');
    // Anuncios
    Route::post('/storeAnuncio', 'AdminController@storeAnuncio');
});

//* Tutor and admin access

Route::group(['middleware' => ['auth:sanctum', 'IsTutorOrAdmin'], 'prefix' => 'tutor'], function () {
//Reportes
Route::get('/{id}/reportesIndividuales', 'TutorController@reportesIndividuales');
Route::get('/{id}/reportesGrupales', 'TutorController@reportesGrupales');

 //alumnos monitores
 Route::get('/crearMonitor', 'TutorController@crearAlumnoMonitor');
 Route::post('/storeMonitor', 'TutorController@storeMonitor');
 Route::get('/{id}/alumnosMonitores', 'TutorController@showAlumnosMonitores');
});

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'monitor'], function () {
    Route::get('/{id}/alumnosTutorados', 'MonitorController@showTutorados');
});



Route::get('/carreras', 'AdminController@getCarreras');
