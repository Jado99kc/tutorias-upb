<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login( Request $request){
        
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if(!Auth::attempt($request->only('email', 'password'))){
            return response([
                'message' => 'Invalid Credentials'
            ], Response::HTTP_UNAUTHORIZED);
        }
        $user = Auth::user();
    
        $token = $user->createToken('token')->plainTextToken;
        $roles = $user->getRoleNames()->toArray();
        $response = [
            'user' => $user,
            'token' => $token,
            'roles' => $roles
        ];
        return response($response, 200);
    }

    public function logout(){
        $cookie = Cookie::forget('jwt');
        request()->user()->currentAccessToken()->delete();
        return response([
            'message' => 'Success',
//            "token" => $user
        ])->withCookie($cookie);
    }

    public function user(){
    // return Auth::user();
    $roles = request()->user()->getRoleNames()->toArray();
    return response() -> json(['user'=> Auth::user(),"roles" => $roles ],Response::HTTP_OK) ;
    }
}
