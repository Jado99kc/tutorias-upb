<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class isadmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
            $roles = request()->user()->getRoleNames()->toArray();
            if (in_array('Admin', $roles)) {
                return $next($request);
            } else {
                return response([
                    'message' => 'Insufficient Permissions'
                ], Response::HTTP_UNAUTHORIZED);
            }
    }
}
