<?php

namespace App\Http\Controllers;

use App\AlumnoMonitor;
use App\Anuncio;
use App\Carrera;
use App\Psicologo;
use App\Tutor;
use App\TutoriaGrupal;
use App\TutoriaIndividual;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    public function __construct()
    {
        //        $this->middleware('auth');
    }

    //perfil
    public function perfil()
    {
        $admin = auth()->user()->admin()->first();
        //        return view('admin.perfil', [
        //            'admin' => $admin
        //        ]);
        return response()->json(['admin' => $admin]);
    }

    public function editPerfil()
    {
        $admin = auth()->user()->admin()->first();
        return view('admin.editPerfil', [
            'admin' => $admin,
        ]);
        //        return response() -> json(['admin'=> $admin]) ;
    }

    public function updatePerfil(Request $request)
    {
        try {
            $tutor = auth()->user()->admin()->first();
            $tutor->nombres = $request->nombres;
            $tutor->apellidoM = $request->apellidoM;
            $tutor->apellidoP = $request->apellidoP;
            $tutor->update();
            if ($request->hasFile('perfil_slug')) {
                $fileName = $request->perfil_slug->getClientOriginalname();
                if (auth()->user()->admin()->first()->perfil_slug) {
                    //                Storage::delete('/public/imagenesPerfil/admin/' . auth()->user()->admin()->first()->perfil_slug);
                    Storage::delete(auth()->user()->admin()->first()->perfil_slug);
                }

                $request->perfil_slug->storeAs('imagenesPerfil/admin', $fileName, 'public');
                //            auth()->user()->admin()->first()->update(['perfil_slug' => $fileName]);
                $route = env('APP_URL') . '/storage/imagenesPerfil/admin/' . $fileName;
                auth()->user()->admin()->first()->update(['perfil_slug' => $route]);
                $tutor->perfil_slug = $route;
            }
            return response([
                'perfil' => $tutor
            ], 200);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //        return redirect('/admin/perfil')->with('msg', 'Perfil actualizado satisfactoriamente.');

    }

    public function getReportes()
    {
        return 'reportes';
    }

    public function crearTutor()
    {
        $carreras = Carrera::all();
        return view('admin.crearTutor', [
            'carreras' => $carreras
        ]);
    }
    public function getCarreras()
    {
        try {
            // $carreras = Carrera::all()->pluck('carrera', "id");
            $carreras = Carrera::select('carrera', 'id')->get()->toArray();
            return response($carreras, Response::HTTP_OK);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function storeTutor(Request $request)
    {
        try {
            //creacion del usuario
            $newUser = new User();
            $newUser->name = $request->nombreUsuario;
            $newUser->email = $request->email;
            $newUser->password = Hash::make($request->password);
            $newUser->assignRole('Tutor');
            $newUser->save();

            //creacion del alumno monitor que corresponde al usuario (perfil)
            $newTutor = new Tutor();
            $newTutor->user_id = $newUser->id;
            $newTutor->nombres = $request->nombres;
            $newTutor->apellidoM = $request->apellidoM;
            $newTutor->apellidoP = $request->apellidoP;
            $newTutor->carrera_id = $request->carrera_id; // era carrera anteriormente
            $newTutor->horario = $request->horario;
            $newTutor->descripcion = $request->descripcion;
            $newTutor->save();

            //        return redirect('/admin/tutores')->with('msg', 'Tutor creado satisfactoriamente.');

            return response($newTutor, Response::HTTP_OK);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showTutores()
    {
        try {
            $tutores = Tutor::paginate(3);

            //    return view('admin.tutores', [
            //        'tutores' => $tutores
            //    ]);
            $paginator = tap($tutores, function ($paginatedInstance) {
                return $paginatedInstance->getCollection()->transform(function ($tutor) {
                    $tutor['alumnosM'] = $tutor->alumnosMonitoresCount();
                    $tutor['carrera'] = Carrera::find($tutor->carrera_id)->carrera;
                    if ($tutor->perfil_slug) {
                        $imagen =  Storage::disk('public')->get($tutor->perfil_slug);
                        $type = pathinfo($imagen, PATHINFO_EXTENSION);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);
                        $tutor['file'] = $base64;
                    }
                    return $tutor;
                });
            });
            // return dd($paginator);
            return response($paginator, Response::HTTP_OK);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function getTutor($id)
    {
        $tutor = Tutor::find($id);
        $tutor['carrera'] = Carrera::find($tutor->carrera_id)->carrera;
        return response($tutor, Response::HTTP_OK);
    }
    public function crearPsicologo()
    {
        return view('admin.crearPsicologo');
    }

    public function storePsicologo(Request $request)
    {

        try {
            //creacion del usuario
            $newUser = new User();
            $newUser->name = $request->nombreUsuario;
            $newUser->email = $request->email;
            $newUser->password = Hash::make($request->password);
            $newUser->assignRole('Psicologo');
            $newUser->save();

            //creacion del psicologo que corresponde al usuario (perfil)
            $newPsicologo = new Psicologo();
            $newPsicologo->user_id = $newUser->id;
            $newPsicologo->nombres = $request->nombres;
            $newPsicologo->apellidoM = $request->apellidoM;
            $newPsicologo->apellidoP = $request->apellidoP;
            //$newPsicologo->horario = $request->horario;
            $newPsicologo->descripcion = $request->descripcion;
            $newPsicologo->activo = 1;
            $newPsicologo->save();

            //            return redirect('/home')->with('msg', 'Psicólogo creado satisfactoriamente.');
            return response([
                'psicologo' => $newPsicologo
            ], 200);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showPsicologos()
    {
        //        return view('admin.psicologos', [
        //            'psicologos' => $psicologos
        //        ]);

        try {
            $psicologos = Psicologo::paginate(15);
            return response($psicologos, 200);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showMonitoresTutor($id)
    {
        $alumnos = AlumnoMonitor::where('tutor_id', '=', $id)->paginate(15);
        $message = 'No hay Alumnos';
        return view('tutor.alumnosMonitores', [
            'alumnos' => $alumnos,
            'message' => $message
        ]);
    }

    public function reportesGrupales()
    {
        try {
            $reportes = TutoriaGrupal::orderBy('created_at', 'DESC')->paginate(2);
            //            return view('admin.reportesGrupales', [
            //                'reportes' => $reportes,
            //            ]);
            $paginator = tap($reportes, function ($paginatedInstance) {
                return $paginatedInstance->getCollection()->transform(function ($reporte) {
                    $reporte['tutor'] = Tutor::find($reporte->tutor_id)->nombreCompleto();
                    $reporte['carrera'] = Carrera::find($reporte->carrera_id)->carrera;
                    return $reporte;
                });
            });
            //            return $paginator;
            return response($paginator, Response::HTTP_OK);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function reportesIndividuales()
    {
        try {
            //            return view('admin.reportesIndividuales', [
            //            'reportes' => $reportes,
            //        ]);
            $reportes = TutoriaIndividual::orderBy('created_at', 'DESC')->paginate(2);
            $paginator = tap($reportes, function ($paginatedInstance) {
                return $paginatedInstance->getCollection()->transform(function ($reporte) {
                    $reporte['tutor'] = Tutor::find($reporte->tutor_id)->nombreCompleto();
                    $reporte['carrera'] = Carrera::find($reporte->carrera_id)->carrera;
                    return $reporte;
                });
            });
            //            return $paginator;
            return response($paginator, Response::HTTP_OK);
        } catch (Throwable $e) {
            return response([
                'error' => $e
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        //
    }

    public function crearAnuncio()
    {
        return view('admin.crearAnuncio');
    }

    public function storeAnuncio(Request $request)
    {
        $anuncio = new Anuncio();
        $anuncio->titulo = $request->titulo;
        $anuncio->descripcion = $request->descripcion;
        $anuncio->link = $request->link;
        if ($request->hasFile('imagen')) {
            $fileName = time() . $request->imagen->getClientOriginalname();
            Storage::disk('public')->put($fileName, File::get($request->imagen));
            $anuncio->imagen = $fileName;
        }
        $anuncio->save();

        //! return image as well
        $imagen =  Storage::disk('public')->get($anuncio->imagen);
        $type = pathinfo($imagen, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);
        $anuncio['file'] = $base64;
        return $anuncio;
        ///////////////////////////////////////////////////////////////////
        return response($anuncio, Response::HTTP_OK);
        // return redirect('/home');
    }

    public function anuncios()
    {
        $anuncios = Anuncio::orderBy('created_at', 'DESC')->where('activo', '=', 1)->paginate(3);
        // return view('admin.anuncios', [
        //     'anuncios' => $anuncios
        // ]);
        $paginator = tap($anuncios, function ($paginatedInstance) {
            return $paginatedInstance->getCollection()->transform(function ($anuncio) {
                if ($anuncio->imagen) {
                    $imagen =  Storage::disk('public')->get($anuncio->imagen);
                    $type = pathinfo($imagen, PATHINFO_EXTENSION);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);
                    $anuncio['file'] = $base64;
                }
                return $anuncio;
            });
        });
        return response($paginator, Response::HTTP_OK);
    }

    public function anuncioEdit($id)
    {
        $anuncio = Anuncio::find($id);
        return view('admin.anuncioEdit', [
            'anuncio' => $anuncio
        ]);
    }

    public function anuncioUpdate(Request $request, $id)
    {
        $anuncio = Anuncio::find($id);
        $anuncio->titulo = $request->titulo;
        $anuncio->descripcion = $request->descripcion;
        $anuncio->link = $request->link;
        $anuncio->update();
        return redirect('/admin/anuncios')->with('msg', 'Anuncio actualizado satistfactoriamente');
    }

    public function anuncioDelete($id)
    {
        $anuncio = Anuncio::find($id);
        $anuncio->forceDelete();
        return redirect('/admin/anuncios')->with('msg', 'Anuncio eliminado satistfactoriamente');
    }

    public function estadisticas(Request $request)
    {
        $nombreTutor = $request->get('tutoBuscarPor');
        $tipoTutor = $request->get('tutorTipo');

        $nombrePsico = $request->get('psicoBuscarPor');
        $tipoPsico = $request->get('psicoTipo');

        $nombreAlum = $request->get('alumBuscarPor');
        $tipoAlum = $request->get('alumTipo');

        $tutores = Tutor::buscarPor($tipoTutor, $nombreTutor)->paginate(5);

        $psicologos = Psicologo::buscarPor($tipoPsico, $nombrePsico)->paginate(5);

        $alumnos = AlumnoMonitor::buscarPor($tipoAlum, $nombreAlum)->paginate(5);


        return view('admin.estadisticas', [
            'tutores' => $tutores,
            'psicologos' => $psicologos,
            'alumnos' => $alumnos
        ]);
    }

    public function reportesTutor(Request $request, $id)
    {
        $tutor = Tutor::where('user_id', '=', $id)->first();
        $tutor_id = $tutor->id;

        $fInicio = $request->get('fI');
        $fFin = $request->get('fF');

        $fInicio2 = $request->get('fI2');
        $fFin2 = $request->get('fF2');

        $reportes = TutoriaGrupal::where('tutor_id', '=', $tutor_id)->orderBy('created_at', 'DESC')->BuscarPor($fInicio, $fFin)->paginate(5);
        $reportes2 = TutoriaIndividual::where('tutor_id', '=', $tutor_id)->orderBy('created_at', 'DESC')->BuscarPor($fInicio2, $fFin2)->paginate(5);
        return view('admin.reportesTutor', [
            'reportes' => $reportes,
            'reportes2' => $reportes2,
            'tutor_id' => $tutor_id
        ]);
    }

    public function editarTutor($id)
    {
        $carreras = Carrera::all();
        $tutor = Tutor::find($id);
        return view('admin.editarTutor', [
            'tutor' => $tutor,
            'carreras' => $carreras
        ]);
    }

    public function actualizarTutor($id, Request $request)
    {
        $tutor = Tutor::find($id);
        $tutor->nombres = $request->nombres;
        $tutor->apellidoM = $request->apellidoM;
        $tutor->apellidoP = $request->apellidoP;
        $tutor->carrera_id = $request->carrera_id;
        $tutor->descripcion = $request->descripcion;
        $tutor->horario = $request->horario;
        $tutor->update();
        return response($tutor, Response::HTTP_OK);
        // return redirect('/admin/tutores')->with('msg', 'Tutor actualizado satisfactoriamente.');
    }

    public function editarPsicologo($id)
    {
        $psicologo = Psicologo::find($id);
        return view('admin.editarPsicologo', [
            'psicologo' => $psicologo,
        ]);
    }

    public function actualizarPsicologo($id, Request $request)
    {
        $psicologo = Psicologo::find($id);
        $psicologo->nombres = $request->nombres;
        $psicologo->apellidoM = $request->apellidoM;
        $psicologo->apellidoP = $request->apellidoP;
        $psicologo->descripcion = $request->descripcion;
        $psicologo->update();
        return redirect('/admin/psicologos')->with('msg', 'Psicólogo actualizado satisfactoriamente.');
    }
    public function getImagen($filename)
    {
        // return $anuncio['file'] = Storage::disk('public')->get('/anuncios/admin/' . $anuncio->imagen);
        $imagen =  Storage::disk('public')->get($filename);
        $type = pathinfo($imagen, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);
        return Response($base64);
    }
}
