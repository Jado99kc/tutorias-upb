<?php

namespace App\Http\Controllers;

use App\AlumnoMonitor;
use App\AlumnoTutorado;
use App\Carrera;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MonitorController extends Controller
{
    public function __construct()
    {

    }

    public function showTutorados($id)
    {
        // $monitor = AlumnoMonitor::find($id);
        // $monitorCarrera = Carrera::find($monitor->carrera_id);
        // $carreraNombre = $monitorCarrera->carrera;
        // $alumnos = AlumnoTutorado::where('alumno_monitor_id', '=', $id)->get();
        // return view('monitor.alumnosTutorados', [
        //     'monitor' => $monitor,
        //     'alumnos' => $alumnos,
        //     'carreraNombre' => $carreraNombre
        // ]);
        $monitor =  AlumnoMonitor::find($id);
        $alumnos_tutorados = $monitor->alumnosTutorados()->where('activo', '=', 1)->paginate(2);

        $paginator = tap($alumnos_tutorados, function ($paginatedInstance) {
            return $paginatedInstance->getCollection()->transform(function ($alumno) {
                $alumno['carrera'] = Carrera::find($alumno->carrera_id)->carrera;
                return $alumno;
            });
        });
        return response($paginator, Response::HTTP_OK);
    }
}
