<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
    protected $table = 'tutores';
    protected $fillable = [
        'perfil_slug'
    ];
    protected $dates = ['created_at', 'updated_at'];

    public  function nombreCompleto()
    {
        $tutor = Tutor::find($this->id);
        $fullName = '' . $tutor->nombres . ' ' . $tutor->apellidoP . ' ' . $tutor->apellidoM;
        return $fullName;
    }

    public function alumnosMonitores()
    {
        return $this->hasMany('App\AlumnoMonitor');
    }
    public function alumnosMonitoresCount()
    {
        $tutor = Tutor::find($this->id);
        return count($tutor->alumnosMonitores->where('activo', '=', 1));
    }

    public function reportesIndividuales(){
        return $this->hasMany('App\TutoriaIndividual');
    }
    public function reportesGrupales(){
        return $this->hasMany('App\TutoriaGrupal');
    }
    public function scopeBuscarPor($query,$tipo,$buscar){
        if( ($tipo) && ($buscar) ){
            return $query->where($tipo, 'like', "%$buscar%");
        }
    }
}
