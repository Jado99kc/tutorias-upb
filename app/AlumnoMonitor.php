<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumnoMonitor extends Model
{
    protected $table = 'alumno_monitor';
    protected $dates = ['created_at', 'updated_at'];

    public function scopeBuscarPor($query,$tipo,$buscar){
        if( ($tipo) && ($buscar) ){
            return $query->where($tipo, 'like', "%$buscar%");
        }
    }
    public function alumnosTutorados(){
        return $this->hasMany('App\AlumnoTutorado',"alumno_monitor_id", "id");
    }
    public function alumnosTutoradosCount()
    {
        $monitor = AlumnoMonitor::find($this->id);
        return count($monitor->alumnosTutorados->where('activo', '=', 1));
    }
}

